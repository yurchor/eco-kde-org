---
title: Energy Efficiency
layout: energy-efficiency
menu:
#   main:
#     name: FEEP
#     weight: 2
---
FEEP is developing tools to improve energy efficiency in free and open source software development. Design and implementation of software has a significant impact on the energy consumption of the systems it is part of. With the right tools, it is possible to quantify and drive down energy consumption. This increased efficiency contributes to a more sustainable use of energy as one of the shared resources of our planet.