---
layout: eco-label
title: Umweltzeichen
---
2020 hat das Umweltbundesamt die Vergabekriterien zum Erlangen des „Blauer Engel“-Nachhaltigkeitslabels für Desktop-Software veröffentlicht. Zu den Kategorien für die Zertifizierung gehören z. B. Energieeffizienz oder die Verlängerung der potenziellen Nutzungsdauer von Hardware und Benutzer*innenautonomie. Die Kriterien sind ohnehin größtenteils schon in Freier und Open-Source-Software umgesetzt.

The BE4FOSS project advances eco-certification for resource efficient software in the FOSS community. Obtaining the Blauer Engel label occurs in 3 steps: (1) MEASURE, (2) ANALYZE, and (3) CERTIFY.

1. MEASURE in dedicated labs, such as at KDAB Berlin
1. ANALYZE using statistical tools such as OSCAR (Open source Software Consumption Analysis in R)
1. CERTIFY by submitting the report on the fulfillment of the Blauer Engel criteria

Die Zertifizierung bringt einige Vorteile mit sich:

- Anerkennung des Erreichens hoher Standards für umweltfreundliches Softwaredesign
- Abgrenzung von freier Software gegenüber Alternativen
- Erhöhung der Attraktivität der Anwendung für Benutzer*innen
- Förderung der Transparenz des ökologischen Fußabdrucks von Software
