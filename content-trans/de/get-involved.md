---
menu:
  main:
    weight: 4
name: KDE Eco
title: Machen Sie mit
userbase: KDE Eco
---
## Werden Sie Teil der Bewegung für nachhaltige Software

Wir brauchen so viele motivierte Menschen wie möglich, um das Projekt voranzutreiben. Hier sind einige Kanäle, über die Sie weitere Informationen erhalten und sich engagieren können.

### Community & Support

- BigBlueButton: Monthly meet-ups, 2nd Wednesdays 19:00 CEST/CET (Berlin time) (contact us for details)
- Matrix-Raum: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Energy Efficiency Mailing list: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE-Eco-Forum: https://forum.kde.org/viewforum.php?f=334

### Resources

- FEEP GitLab repository: https://invent.kde.org/teams/eco/feep
- BE4FOSS GitLab repository: https://invent.kde.org/teams/eco/be4foss
- Blue Angel Applications: https://invent.kde.org/teams/eco/blue-angel-application

### Kontakt

Email: `joseph [at] kde.org`
