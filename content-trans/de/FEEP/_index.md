---
layout: energy-efficiency
menu: ''
title: Energieeffizienz
---
FEEP entwickelt Tools zur Verbesserung der Energieeffizienz bei der Entwicklung Freier und Open-Source-Software. Die Gestaltung und Implementierung von Software haben einen erheblichen Einfluss auf den Energieverbrauch der Systeme, zu denen sie gehört. Mit den richtigen Werkzeugen ist es möglich, den Energieverbrauch zu quantifizieren und zu senken. Diese höhere Effizienz trägt zu einer nachhaltigeren Nutzung der Energie als einer der gemeinsamen Ressourcen unseres Planeten bei.
