---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Software Libre
- Eficiencia energética
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: La eficiencia del software es la eficiencia de los recursos!
title: Software de uso eficiente de los recursos y ecocertificación Blauer Engel
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
A menudo se pasa por alto el hecho de que el consumo de recursos del hardware viene determinado por el software que se ejecuta en él.

*¿Cuánta energía consume el hardware?*

Fíjese en el software: la misma máquina haciendo la misma tarea pero con dos aplicaciones diferentes puede tener [demandas de energía drásticamente diferentes](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*¿Cuánto dura la vida útil de una pieza de hardware?*

De nuevo, el software es crítico. La sobrecarga de software, que requiere un hardware cada vez más potente, hace que se fabriquen y vendan nuevos dispositivos, mientras que otros que funcionan perfectamente se desechan.

{{< container class="text-center" >}}

![WEEE Man At The Eden Project](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Así que al pensar en qué hacer mientras la crisis climática continúa, por dónde empezar y cómo, en [KDE Eco](https://eco.kde.org/) pensamos que el software -- especialmente el Software Libre -- es un buen punto de partida. Software Libre significa que los usuarios y sus comunidades tienen la libertad de controlar el software que utilizan, y no al revés. Estas libertades proporcionan una elección real. Elección de qué instalar o desinstalar. Opciones para modificar el software para que funcione de forma más eficiente. Soporte continuo para dispositivos más antiguos, pero que funcionan perfectamente. Y así sucesivamente.

Cada una de estas opciones tiene un coste y/o un beneficio. Por ejemplo, cuantos más procesos (innecesarios) que una aplicación tiene en segundo plano, más recursos necesitará el hardware. Multiplique estos costes por cientos de millones, o incluso miles de millones, de usuarios de ordenadores en todo el mundo, y se acumula rápidamente.

Lo contrario también es cierto: si se reducen las demandas de software, se reducen la demanda de nuestros recursos.

En otras palabras, ¡la eficiencia del software significa eficiencia de los recursos!

Recientemente, la interdependencia entre la ingeniería de software y la sostenibilidad fue reconocida oficialmente por la [Agencia Alemana del Medio Ambiente](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, o UBA). En 2020, UBA publicó los [criterios de adjudicación](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) para obtener la ecocertificación con la etiqueta *Blauer Engel* para software de escritorio. Las categorías para la certificación incluyen la eficiencia energética, la prolongación de la vida útil del hardware y la autonomía del usuario, encajando todas a la perfección con el software libre de código abierto.

{{< container class="text-center" >}}

![Logo KDE Eco con vegetación](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

El proyecto *Blauer Engel 4 FOSS* ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) de KDE e.V. busca recopilar, resumir y difundir información relacionada con la ecocertificación Blauer Engel y la eficiencia de los recursos en relación con el desarrollo de FOSS. Medir el consumo de energía del Software Libre es el enfoque del Proyecto de Eficiencia Energética de software libre y código abierto([FEEP](https://invent.kde.org/teams/eco/feep)). ¡Ambos proyectos son parte de la iniciativa pionera [KDE Eco](https://invent.kde.org/teams/eco)!

{{< container class="text-center" >}}

<font size="6"> **[[¡Únase a nosotros en la construcción de un software libre energéticamente eficiente!](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*Esta es una versión modificada del resumen [presentado](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) al escenario remoto de Rhein Ruhr en el CCC 2021. Puede ver la charla [aquí](https://streaming.media.ccc.de/rc3/relive/319).*

#### Aviso de financiación

El proyecto BE4FOSS fue financiado por la Agencia Federal de Medio Ambiente y el Ministerio Federal de Medio Ambiente, Protección de la Naturaleza, Seguridad Nuclear y Protección al Consumidor (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). Los fondos se facilitan mediante una resolución del Parlamento Federal Alemán.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="logo BMUV" width="340px"/>

<img src="/blog/images/uba.jpg" alt="logo UBA" width="250px"/>

{{< /container >}}

El editor es responsable del contenido de esta publicación.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
