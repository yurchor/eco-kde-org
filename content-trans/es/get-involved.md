---
menu:
  main:
    weight: 4
name: KDE Eco
title: Implicarse
userbase: KDE Eco
---
## Forme parte del movimiento de software sostenible

Necesitamos el mayor número posible de personas motivadas para impulsarlo. Aquí tiene algunos canales en los que puede obtener más información y contribuir.

### Comunidad y soporte

- BigBlueButton: Reuniones mensuales, 2.º miércoles del mes a las 19:00 CEST/CET (hora de Berlín) (contáctenos para más detalles)
- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Lista de correo de Energy Efficiency: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Foro de KDE Eco: https://forum.kde.org/viewforum.php?f=334

### Recursos

- Repositorio GitLab de FEEP: https://invent.kde.org/teams/eco/feep
- Repositorio GitLab de BE4FOSS: https://invent.kde.org/teams/eco/be4foss
- Solicitudes para *Blue Angel* : https://invent.kde.org/teams/eco/blue-angel-application

### Contacto

Correo electrónico: `joseph [at] kde.org`
