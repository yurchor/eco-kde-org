---
menu:
  main:
    weight: 4
name: KDE Eco
title: Envolva-se
userbase: KDE Eco
---
## Torne-se parte do movimento de 'software' sustentável

Precisamos de muita gente o mais motivada possível para levar isto em diante. Aqui estão alguns canais onde poderá obter mais informações e contribuir.

### Community & Support

- BigBlueButton: Monthly meet-ups, 2nd Wednesdays 19:00 CEST/CET (Berlin time) (contact us for details)
- Sala do Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Energy Efficiency Mailing list: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Fórum do KDE Eco: https://forum.kde.org/viewforum.php?f=334

### Resources

- FEEP GitLab repository: https://invent.kde.org/teams/eco/feep
- BE4FOSS GitLab repository: https://invent.kde.org/teams/eco/be4foss
- Blue Angel Applications: https://invent.kde.org/teams/eco/blue-angel-application

### Contacto

E-mail: `joseph [at] kde.org`
