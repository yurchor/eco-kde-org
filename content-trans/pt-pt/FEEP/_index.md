---
layout: energy-efficiency
menu: ''
title: Eficiência Energética
---
A FEEP está a desenvolver ferramentas para melhor a eficiência energética no desenvolvimento de 'software' livre e de código aberto. O desenho e a implementação do 'software' tem um impacto significativo no consumo de energia dos sistemas onde está instalado. Com as ferramentas correctas, é possível quantificar e descodificar o consumo de energia. Este aumento de eficiência contribui para uma utilização mais sustentável da energia como um dos recursos partilhados no nosso planeta.
