---
layout: eco-label
title: Emblema Eco
---
Em 2020, a Umweltbundesamt ('Agência Ambiental Alemã') lançou os critérios para a obtenção de certificações ecológicas com o emblema Blauer Engel para o 'software' de computadores. As categorias de certificação incluem a eficiência energética, a extensão do tempo de vida potencial do 'hardware' e a autonomia do utilizador … as quais se encaixam perfeitamente no 'software' livre e de código-aberto.

The BE4FOSS project advances eco-certification for resource efficient software in the FOSS community. Obtaining the Blauer Engel label occurs in 3 steps: (1) MEASURE, (2) ANALYZE, and (3) CERTIFY.

1. MEASURE in dedicated labs, such as at KDAB Berlin
1. ANALYZE using statistical tools such as OSCAR (Open source Software Consumption Analysis in R)
1. CERTIFY by submitting the report on the fulfillment of the Blauer Engel criteria

Os benefícios de obter este emblema incluem:

- Reconhecimento no alcance de padrões elevados no desenho de 'software' amigo do ambiente
- Diferenciação do 'software' livre face às alternativas
- Aumento do apelo à adopção pelos consumidores
- Promoção da transparência na pegada ecológica das aplicações
