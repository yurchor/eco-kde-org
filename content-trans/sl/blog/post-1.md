---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Prosto programje
- Energetska učinkovitost
- Hardware Operating Life
- Modri angel
date: 2022-01-25
summary: Programska učinkovitost je učinkovita raba virov!
title: Eko potrdilo Modri angel za programje, ki učinkovito uporablja vire
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
Pogosto je spregledano dejstvo, da je poraba virov strojne opreme določena s programsko opremo, ki se izvaja na njej.

*Koliko energije uporablja strojna oprema?*

Oglejte si programsko opremo: isti računalnik opravlja isto nalogo, vendar dve različni aplikaciji imata [drastično različne zahteve po energiji](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*Kako dolga je življenjska doba strojne opreme?*

Spet je kritična programska oprema. Potratna programska oprema zahteva več in močnejšo strojno opremo v novih napravah, ki se proizvajajo in prodajajo, medtem ko se popolnoma delujoče naprave zavržejo.

{{< container class="text-center" >}}

![WEEE Man At The Eden Project](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Torej, ko razmišljate o tem, kaj storiti, ko se podnebna kriza nadaljuje, kje začeti in kako, na [KDE Eco](https://eco.kde.org/) menimo, da je programje - posebej prosta programska oprema - dober kraj za začetek. Prosta programska oprema pomeni, da imajo uporabniki in njihove skupnosti svobodo nadzora programske opreme in ne obratno. Te svoboščine zagotavljajo resnično izbiro. Izbira kaj namestiti ali odstraniti. Možnosti za spremembo programske opreme da deluje bolj učinkovito. Nadaljnjo podporo starejšim, a popolnoma delujočim napravam. In tako naprej.

Vsaka od teh odločitev ima strošek in/ali korist. Na primer, več (nepotrebnih) postopkov, ki jih aplikacija poganja v ozadju, več virov bo potrebovala strojna oprema. Te stroške pomnožite na stotine milijone ali celo milijarde uporabnikov računalnikov po vsem svetu in ti se hitro povečujejo.

Velja tudi obratno: zmanjšane zahteve iz programske opreme zmanjšujejozahteve po naših virih.

Z drugimi besedami, učinkovitost programske opreme pomeni učinkovitost virov!

Pred kratkim, je medsebojno odvisnost med programsko opremo intrajnostjo uradno priznala [Nemška agencija za okolje](https://www.umweltbundesamt.de/en) (* umweltbundesamt *, ali UBA). Leta 2020 je UBA izdala [merila za dodelitev] (https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) za namizno programsko opremo za pridobitev eko-certifikata z oznako Modri angel. Kategorije za potrjevanje vključujejo energetsko učinkovitost, razširitev potencialne življenjske dobe strojne opreme in avtonomije uporabnikov ... vse se brezhibno prilegajo prosti in odprtokodni programski opremi (angl. FOSS).

{{< container class="text-center" >}}

![KDE Eco logotip z vegetacijo](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

Projekt Modri angel - Blauer Engel 4 FOSS ([BE4Foss](https://invent.kde.org/teams/eco/be4Foss)) skupnosti KDE e.V. skuša zbrati, povzeti in razširiti informacije, povezane z eko-potrjevanjem Modri angel in učinkovitosti virov, kot se nanaša na razvoj FOSS. Merjenje porabe energije proste programske opreme je poudarek proste in odprtokodne programske opreme. Projekt energetske učinkovitosti ([FEEP](https://invent.kde.org/teams/eco/Feep)).Oba projekta sta del pionirske pobude [KDE ECO](https://invent.kde.org/teams/eco) !

{{< container class="text-center" >}}

<font size="6">**[pridružite se nam pri razvoju energetsko učinkovite proste programske opreme](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*To je spremenjena verzija povzetka [predloženega](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) na Remote Rhein Ruhr Stage at CCC 2021. Govor si lahko ogledate [tukaj](https://streaming.media.ccc.de/rc3/relive/319).*

#### Obvestilo o financiranju

Projekt BE4Foss je financirala nemška Zvezna agencija za okolje in Zvezno ministrstvo za okolje, ohranjanje narave, jedrsko varnost in varstvo potrošnikov (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). Sredstva so na voljo z resolucijo nemškega Bundestaga.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

Založnik je odgovoren za vsebino te publikacije.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
