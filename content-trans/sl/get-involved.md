---
menu:
  main:
    weight: 4
name: KDE Eco
title: Bodite vpleteni
userbase: KDE Eco
---
## Postanite del gibanja za trajnostno programsko opremo

Potrebujemo kar največ motiviranih ljudi, ki bodo poganjali razvoj naprej. Tukaj je nekaj kanalov, kjer lahko dobite več informacij in lahko prispevate.

### Skupnosti in podpora

- BigBlueButton : Mesečni sestanki, druga sreda ob 19:00 CEST/CET (slovenski čas) (stopite z nami v stik za podrobnosti)
- Matrix soba: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Poštni seznam energetske učinkovitosti: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

### Viri

- Repozitorij FEEP GitLab: https://invent.kde.org/teams/eco/feep
- Repozitorij BE4FOSS GitLab: https://invent.kde.org/teams/eco/be4foss
- Aplikacije Modri angel: https://invent.kde.org/teams/eco/blue-angel-application

### Kontakt

E-pošta: `joseph [at] kde.org`
