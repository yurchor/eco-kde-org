---
menu:
  main:
    weight: 4
name: KDE Eco
title: Engagera dig
userbase: KDE Eco
---
## Bli en del av rörelsen för hållbar programvara

Vi behöver så många motiverade personer som möjligt för att komma framåt. Här är några kanaler där du kan få mer information och bidra.

### Gemenskap och stöd

- BigBlueButton: Månadsvisa träffar, andra onsdagen 19:00 CEST/CET (Berlintid, kontakta oss för detaljerad information)
- Matrix-rum: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- E-postlista för energieffektivitet: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE miljöforum: https://forum.kde.org/viewforum.php?f=334

### Resurser

- FEEP GitLab-arkiv: https://invent.kde.org/teams/eco/feep
- BE4FOSS GitLab-arkiv: https://invent.kde.org/teams/eco/be4foss
- Blue Angel-program: https://invent.kde.org/teams/eco/blue-angel-application

### Kontakter

E-post: `joseph [snabela] kde.org`
