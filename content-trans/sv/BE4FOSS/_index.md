---
layout: eco-label
title: Miljömärkning
---
Under 2020 gav Umweltbundesamt ('Tyska naturvårdsverket') ut kriterier för tilldelning av miljöcertifieringen med beteckningen Blauer Engel för skrivbordsprogramvara. Kategorier för certifiering omfattar energieffektivitet, förlängning av den potentiella livslängden hos maskinvara, och användarautonomi … som alla smidigt passar ihop med fri och öppen källkod.

Projektet BE4FOSS utvecklar miljöcertifiering för resurseffektiv programvara i FOSS-gemenskapen. Att erhålla beteckningen Blauer Engel label görs i tre steg: (1) MÄT, (2) ANALYSERA, och (3) CERTIFIERA.

1. MÄT i dedikerade laboratorier, såsom i KDAB Berlin
1. ANALYSERA genom att använda statistiska verktyg såsom OSCAR (Open source Software Consumption Analysis in R)
1. CERTIFIERA genom att skicka in en rapport om hur Blauer Engel kriterierna uppfylls

Fördelarna med att erhålla miljömärkningen omfattar:

- Erkännande av att ha nått en hög standard av miljövänlig programvarukonstruktion
- Särskiljande av fri programvara från alternativen
- Ökning av hur attraktiv användning är för brukare
- Främjande av transparens rörande det ekologiska avtrycket hos programvara
