---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Fri programvara
- Energieffektivitet
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: Programvarueffektivitet är resurseffektivitet!
title: Resurseffektiv programvara och Blauer Engel eko-certifiering
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
Det som oftast förbises är det faktum att maskinvarans resursanvändning bestäms av programvaran som kör på den.

*Hur mycket energi använder maskinvara?*

Ta en titt på programvaran: samma dator som utför samma uppgift men med två olika program kan ha [drastiskt olika energibehov](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*Hur lång är användningslivslängden för maskinvara?*

Återigen, programvara är kritisk. Svällande programvara som kräver mer och mer kraftfull leder till att nya apparater tillverkas och säljes, medan helt fungerande apparater kastas.

{{< container class="text-center" >}}

![Eden-projektets WEEE-man](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Så när man tänker på vad man ska göra medan klimatkrisen fortsätter, var man ska börja och hur, tror vi på [KDE Eco](https://eco.kde.org/) att programvara, i synnerhet fri programvara, är ett bra ställe att börja. Fri programvara betyder att användare och deras gemenskaper har frihet att kontrollera programvaran de använder, inte tvärtom. Friheten leder till verkliga valmöjligheter. Möjlighet att välja vad man installerar, eller avinstallerar. Alternativ för att ändra programvara så att den blir effektivare. Fortsatt stöd för äldre, men hel funktionsdugliga apparater. Och så vidare.

Var och en av valmöjligheterna har en kostnad och/eller fördel. Exempelvis, ju fler (onödiga) processer som ett program kör i bakgrunden, desto mer resurser krävs av maskinvaran. Multiplicera sådana kostnader med hundratals miljoner, eller till och med miljarder, datorer över hela världen, så ökar de snabbt.

Det omvända är också sant: reducerar man kraven från programvara så reducerar man kraven på våra resurser.

Med andra ord, programvarueffektivitet betyder resurseffektivitet!

Nyligen erkändes beroendet mellan programvarukonstruktion och hållbarhet officiellt av [Tyska naturvårdsverket](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, eller UBA). Under 2020 gav UBA ut [kriterier för tilldelning av miljöcertifiering](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) för skrivbordsprogramvara med beteckningen Blauer Engel. Kategorier för certifiering omfattar energieffektivitet, förlängning av den potentiella livslängden hos maskinvara, och användarautonomi … som alla smidigt passar ihop med fri och öppen källkod.

{{< container class="text-center" >}}

![KDE Eco-logotyp med vegetation](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

Projektet Blauer Engel 4 FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) från KDE e.V. har som avsikt att samla, sammanfatta och sprida information om Blauer Engel eko-certifiering och resurseffektivitet när det gäller utveckling av fri och öppen programvara. Att mäta energianvändningen för fri programvara är fokus för projektet för energieffektivitet hos fri och öppen programvara ([FEEP](https://invent.kde.org/teams/eco/feep)). Båda projekt ingår i det banbrytande [KDE Eco](https://invent.kde.org/teams/eco) initiativet.

{{< container class="text-center" >}}

<font size="6">**[Hjälp oss bygga energieffektiv fri programvara.](https://eco.kde.org/get-involved/)**</font>

{{< /container >}}

*Det är en modifierad version av [inskickat sammandrag](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) till Remote Rhein Ruhr scenen i CCC 2021. Du kan titta på föredraget [här](https://streaming.media.ccc.de/rc3/relive/319).*

#### Finansieringsanmärkning

Projektet BE4FOSS grundades av det federala miljödepartementet och det federala ministeriet för miljö, naturvård, kärnkraftssäkerhet och konsumentskydd (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</sup>). Medel tillhandahålls genom beslut av den Tyska Förbundsdagen.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

Utgivaren är ansvarig för innehållet i den här publikationen.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
