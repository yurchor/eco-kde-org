---
menu:
  main:
    weight: 4
name: KDE Eco
title: Участь у команді
userbase: KDE Eco
---
## Станьте частиною руху стійкого програмного забезпечення

Нам потрібно якомога більше мотивованих людей для просування ідеї. Нижче наведено деякі канали, на яких ви можете отримати додаткові відомості та взяти участь у роботі.

### Підтримка і спільнота

- BigBlueButton: щомісячні зустрічі, друга середа, 19:00 CEST/CET (берлінський час) (зв'яжіться з нами, щоб дізнатися більше)
- Кімната Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Список листування з енергоефективності: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Форум KDE Eco: https://forum.kde.org/viewforum.php?f=334

### Ресурси

- Сховище GitLab FEEP: https://invent.kde.org/teams/eco/feep
- Сховище GitLab BE4FOSS: https://invent.kde.org/teams/eco/be4foss
- Програми Blue Angel: https://invent.kde.org/teams/eco/blue-angel-application

### Контакт

Ел. пошта: `joseph [at] kde.org`
