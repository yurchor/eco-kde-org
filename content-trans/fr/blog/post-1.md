---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Logiciels libres
- Efficacité énergétique
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: L'efficacité des logiciels est l'efficacité des ressources! 
title: Logiciel économe en ressources et éco-certification « Blauer Engel »
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
On néglige souvent le fait que la consommation de ressources pour les composants matériels est déterminée par les logiciels qui les exploitent.

*Combien d'énergie utilise le matériel informatique ? *

Prenons un moment pour analyser les logiciels : la même machine effectuant la même tâche mais avec mais avec deux applications différentes peut avoir [des besoins énergétiques radicalement différents](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

Quelle est la durée de vie opérationnelle d'un composant matériel ? *

Là encore, les logiciels sont essentiels. L'accumulation de logiciels nécessitant un matériel de plus en plus puissant conduisent à la fabrication et à la vente de nouveaux appareils, alors que des appareils parfaitement fonctionnels sont mis au rebut.

{{< container class="text-center" >}}

![« WEEE Man » du projet « The Eden »](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg « « WEEE Man » du projet « The Eden »)

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Donc, en réfléchissant à ce qu'il faut faire pendant que la crise climatique continue, par où commencer et comment. Pour la communauté [KDE Eco] (https://eco.kde.org/), nous pensons que les logiciels -- en particulier les logiciels libres - est un excellent point de départ. Les logiciels libres signifient que les personnes utilisatrices et leurs communautés possèdent la liberté de contrôler les logiciels qu'ils utilisent et non l'inverse. Ces libertés offrent un véritable choix. Le choix de ce qu'il faut installer ou désinstaller, des options pour modifier les logiciels pour qu'ils fonctionnent plus efficacement, une prise en charge continue pour les logiciels anciens mais parfaitement fonctionnels et ainsi de suite.

Chacun de ces choix conduit à un coût et / ou un avantage. Par exemple, plus le nombre de processus (inutiles) qu'une application exécute en arrière-plan, plus le matériel consommera de ressources. Multipliez ces coûts par des centaines de millions, voire des milliards, d'utilisateurs d'ordinateurs dans le monde et les coûts s'envolent rapidement.

L'inverse est également vrai : en réduisant les demandes venant des logiciels, on réduit les demandes sur nos ressources.

En d'autres termes, l'efficacité des logiciels est synonyme d'efficacité des ressources !

Récemment, l'interdépendance entre le génie logiciel et la durabilité a été officiellement reconnue par l'Agence Allemande pour l'environnement (https://www.umweltbundesamt.de/en)(*Umweltbundesamt* ou UBA). En 2020, cette agence a publié les [critères d'attribution](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) pour l'obtention de l'écocertification avec le label « Blauer Engel » pour les logiciels de bureau. Les catégories de certification englobent l'efficacité énergétique, l'allongement de la durée de vie potentielle du matériel et l'autonomie de l'utilisateur... autant d'éléments s'intégrant parfaitement aux logiciels libres et « Open source ».

{{< container class="text-center" >}}

![Logo de KDE Eco logo avec la végétation](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

Le projet « Blauer Engel 4 FOSS » ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) de KDE e.V. cherche à collecter, résumer et diffuser les informations relatives à l'éco-certification et à l'efficacité des ressources pour la certification « Blauer Engel », puisque cela concerne le développement des logiciels libres (« FOSS », Free & Open Source Software). La mesure de la consommation d'énergie des logiciels libres est l'objectif du projet ([FEEP](https://invent.kde.org/teams/eco/feep))(Free & Open Source Software Energy Efficiency Project). Les deux projets font partie de l'initiative innovante [KDE Eco](https://invent.kde.org/teams/eco) !

{{< container class="text-center" >}}

<font size="6"> **[Rejoignez-nous pour construire des logiciels libres et économes en énergie ! ](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*Ceci est une version modifiée du résumé [présenté](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) à la réunion en ligne « Rhein Ruhr Stage au CCC 2021 ». Vous pouvez revoir la conférence [ici](https://streaming.media.ccc.de/rc3/relive/319). *

#### Modalités pour le financement

Le projet « BE4FOSS » a été financé par l'Agence fédérale pour l'environnement et le ministère fédéral de l'environnement, de la protection de la nature, de la sécurité nucléaire et de la protection des consommateurs (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</sup>). Les fonds sont mis à disposition par une résolution par le Bundestag, le parlement allemand.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

L'éditeur est responsable du contenu de cette publication.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
