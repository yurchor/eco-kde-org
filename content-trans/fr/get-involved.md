---
menu:
  main:
    weight: 4
name: KDE Eco
title: Impliquez-vous
userbase: KDE Eco
---
## Devenir une partie du mouvement pour des logiciels durables

Nous avons besoin du plus grand nombre possible de personnes motivées pour faire avancer ce projet. Voici quelques canaux où vous pouvez obtenir plus d'informations et contribuer.

### Communauté et prise en charge

- BigBlueButton : réunions mensuelles, chaque second mercredi à 19:00 CEST / CET (Fuseau Allemagne) (Contactez nous pour plus de détails)
- Salon « Matrix » : https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Liste de diffusion sur l'efficacité énergétique : https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Forum de KDE Eco : https://forum.kde.org/viewforum.php?f=334

### Ressources

- Dépôt « FEEP GitLab » : https://invent.kde.org/teams/eco/feep
- Dépôt « BE4FOSS GitLab » : https://invent.kde.org/teams/eco/be4foss
- Applications « Blue Angel » : https://invent.kde.org/teams/eco/blue-angel-application

### Contact

Courriel : « joseph [at] kde.org »
