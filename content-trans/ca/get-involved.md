---
menu:
  main:
    weight: 4
name: KDE Eco
title: Com participar
userbase: KDE Eco
---
## Formeu part del moviment de programari sostenible

Necessitem tanta gent motivada com sigui possible per a impulsar aquest repte. Aquí hi ha diversos canals a on podeu obtenir més informació i col·laborar.

### Comunitat i assistència

- BigBlueButton: Trobades mensuals, 2n dimecres 19:00 CEST/CET (hora Berlín) (contacteu amb nosaltres per als detalls)
- Sala de Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Llista de correu d'eficiència energètica: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- Fòrum de KDE Eco: https://forum.kde.org/viewforum.php?f=334

### Recursos

- Repositori GitLab del FEEP: https://invent.kde.org/teams/eco/feep
- Repositori GitLab del BE4FOSS: https://invent.kde.org/teams/eco/be4foss
- Aplicacions de Blue Angel: https://invent.kde.org/teams/eco/blue-angel-application

### Contact

Correu: `joseph [at] kde.org`
