---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Programari lliure
- Eficiència energètica
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: L'eficiència del programari és l'eficiència de recursos!
title: Programari eficient en recursos i ecocertificació Blauer Engel
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
Sovint es passa per alt el fet que el consum de recursos del maquinari està determinat pel programari que s'executa en ell.

*Quanta energia usa el maquinari?*

Donem un cop d'ull al programari: la mateixa màquina que fa la mateixa tasca però amb dues aplicacions diferents pot tenir [exigències energètiques molt diferents](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*Quant dura la vida operativa d'una peça de maquinari?*

De nou, el programari és crític. La inflació de programari que requereix un maquinari més i més potent condueix a la fabricació i venda de dispositius nous, mentre que els dispositius que funcionen perfectament es descarten.

{{< container class="text-center" >}}

![WEEE Man At The Eden Project](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Quan pensem en què fer mentre continua la crisi climàtica, per on començar i com, a [KDE Eco](https://eco.kde.org/) pensem que el programari, especialment el programari lliure, és un bon lloc per començar. El programari lliure significa que els usuaris i les seves comunitats tenen la llibertat de controlar el programari que utilitzen, no a l'inrevés. Aquestes llibertats ofereixen una selecció real. Trieu què instal·lar o desinstal·lar. Les opcions per modificar el programari per executar-se de forma més eficient. Suport continuat per a dispositius més antics, però perfectament funcionals. I així successivament.

Cadascuna d'aquestes opcions té un cost o benefici. Per exemple, com més processos (innecessaris) una aplicació està executant en segon pla, més recursos de maquinari necessitarà. Multipliqueu aquests costos per centenars de milions, o fins i tot milers de milions, d'usuaris d'ordinador a tot el món, i creix ràpidament.

El contrari també és cert: en reduir les demandes del programari es redueixen les demandes dels nostres recursos.

En altres paraules, eficiència del programari vol dir eficiència de recursos!

Recentment, s'ha reconegut oficialment la interdependència entre l'enginyeria de programari i la sostenibilitat per l'[Agència Alemanya del Medi Ambient](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, o UBA). En 2020, l'UBA va publicar els [criteris de concessió](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) per obtenir l'ecocertificació amb l'etiqueta Blauer Engel del programari d'escriptori. Les categories de la certificació inclouen l'eficiència energètica, ampliació de la vida operativa potencial del maquinari, i l'autonomia de l'usuari… tot això s'adapta sense problemes al programari de codi lliure i obert (FOSS).

{{< container class="text-center" >}}

![Logotip de KDE Eco amb vegetació](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

El projecte Blauer Engel 4 FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) de KDE e.V. busca recollir, resumir i difondre informació relacionada amb l'ecocertificació Blauer Engel i l'eficiència de recursos, ja que es refereix al desenvolupament de FOSS. Mesurar el consum d'energia del programari lliure és el centre del Free & Open Source Software Energy Efficiency Project ([FEEP](https://invent.kde.org/teams/eco/feep)). Ambdós projectes formen part de la iniciativa pionera [KDE Eco](https://invent.kde.org/teams/eco)!

{{< container class="text-center" >}}

<font size="6"> **[Uniu-vos a construir programari lliure energèticament eficient.](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*Aquesta és una versió modificada del resum [tramés](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) al Remote Rhein Ruhr Stage del CCC 2021. Podeu veure la xerrada [aquí](https://streaming.media.ccc.de/rc3/relive/319).*

#### Anunci de finançament

El projecte BE4FOSS ha estat finançat per l'Agència Federal del Medi Ambient i el Ministeri Federal de Medi Ambient, Conservació de la Naturalesa, Seguretat Nuclear i Protecció del Consumidor (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>). Els fons estan disponibles per una resolució del Bundestag alemany.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="Logotip del BMUV" width="340px"/>

<img src="/blog/images/uba.jpg" alt="Logotip de la UBA" width="250px"/>

{{< /container >}}

L'editor és el responsable del contingut d'aquesta publicació.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
