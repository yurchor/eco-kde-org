---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Vrije software
- Energie-efficiëntie
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: Software efficiëntie is hulpbron efficiëntie!
title: Hulpbron-efficiënte software & Blauer Engel Eco-Certificatie
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
Er is vaak over het feit heen gekeken dat de hulpbronconsumptie van hardware bepaald wordt door de software die er op draait.

*Hoeveel energie gebruikt de hardware?*

Neem een kijkje in de software: dezelfde machine die dezelfde taak uitvoert maar met twee verschillende toepassingen kan [drastisch verschillende energiebehoeften](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018-12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf) hebben.

*Hoe lang is de levensduur van een stukje hardware?*

Nog eens, software is kritisch. Software opblazen vereist meer en meer krachtige hardware, wat resulteert in nieuw gemaakte en verkochte apparaten, terwijl perfect functionerende apparaten worden afgedankt.

{{< container class="text-center" >}}

![WEEE Man At The Eden Project](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man At The Eden Project")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Dus bij het denken over wat te doen terwijl de klimaatcrisis doorgaat, waar te starten en hoe, denken wij bij [KDE Eco](https://eco.kde.org/) over software -- speciaal Vrije Software -- is een goede plek om te starten. Vrije Software betekent gebruikers en hun gemeenschappen hebben de vrijheid om de software die ze gebruiken te controleren, niet andersom. Deze vrijheden bieden echte keuze. Keuze in wat te installeren of niet te installeren. Opties om software te wijzigen om efficiënter te draaien. Ondersteuning voor oudere, maar perfect functionerende apparaten te continueren. Enzovoort.

Elk van deze keuzes hebben kosten en/of voordelen. Bijvoorbeeld, hoe meer (onnodige) processen een toepassing in de achtergrond heeft draaien, hoe meer hulpbronnen de hardware nodig zal hebben. Vermenigvuldig zulke kosten met honderden miljoenen of zelfs biljoenen, computergebruikers wereldwijd, en het telt snel op.

Het tegenovergestelde is ook waar: reduceer de eisen van software en dat reduceert de eisen op onze hulpbronnen.

In andere woorden, software efficiëntie betekent hulpbronefficiëntie!

Recent is de onderlinge afhankelijkheid tussen software engineering en onderhoudbaarheid officieel erkend door de [German Environment Agency](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, or UBA). In 2020, gaf UBA de [criteria voor prijsuitreiking](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) uit voor bureaubladsoftware om eco-certificatie met het label Blauer Engel. Categorieën voor certificatie omvatten energie-efficiëntie, die de potentiële levensduur van hardware verlengt, en autonomie van de gebruiker ... waarvan alles naadloos past in Vrije en open-source Software (FOSS).

{{< container class="text-center" >}}

![KDE Eco-logo met vegetatie](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

Het project Blauer Engel 4 FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) van KDE e.V. zoekt het verzamelen, samenvatten en verspreiden van informatie gerelateerd aan eco-certificatie van Blauer Engel en hulpbronefficiëntie zoals het verband houdt met FOSS ontwikkeling. Meten van de energieconsumptie van Vrije Software is de focus van de Vrije & Open-source Software Energie-efficiëntie project ([FEEP](https://invent.kde.org/teams/eco/feep)). Beide projecten zijn onderdeel van het pioneering [KDE Eco](https://invent.kde.org/teams/eco) initiatief!

{{< container class="text-center" >}}

<font size="6"> **[Doe mee met ons in het bouwen van energie-efficiënte, Vrije Software.](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*Dit is een gewijzigde versie van de samenvatting [submitted](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) naar de Remote Rhein Ruhr Stage op CCC 2021. U kunt de praatjes [hier](https://streaming.media.ccc.de/rc3/relive/319) bekijken.*

#### Financieringsopmerking

Het project BE4FOSS kreeg fondsen van de Federale Omgevingsagency en het Federale Ministerie voor de omgeving, Natuurbeheer, Nucleaire veiligheid en Consumentenbescherming (BMUV<sup><a id="fnr.1" class="footref" href="#fn.1">1</sup>). De fondsen zijn beschikbaar gesteld door een resolutie van de Duitse Bondsdag.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV-logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA-logo" width="250px"/>

{{< /container >}}

De uitgever is verantwoordelijk voor de inhoud van deze publicatie.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
