---
menu:
  main:
    weight: 4
name: KDE Eco
title: Doe mee
userbase: KDE Eco
---
## Wordt een onderdeel van de duurzame software beweging

We hebben zoveel mogelijk personen nodig om dit vooruit te brengen. Hier zijn enige kanalen waar u meer informatie kan krijgen en bijdragen.

### Gemeenschap & ondersteuning

- BigBlueButton : Maandelijkse ontmoetingen, tweede woensdag 19:00 CEST/CET (Berlijnse tijd)(neem contact op met ons voor details)
- Matrix-room: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Energie-efficiëntie e-maillijst: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE Eco-forum: https://forum.kde.org/viewforum.php?f=334

### Hulpbronnen

- FEEP GitLab opslagruimte: https://invent.kde.org/teakms/eco/feep
- BE4FOSS GitLab opslagruimte: https://invent.kde.org/teams/eco/be4foss
- Blue Angel toepassingen: https://invent.kde.org/teams/eco/blue-angel-application

### Contact

E-mail: `joseph [at] kde.org`
