---
menu:
  main:
    weight: 4
name: KDE Eco
title: 참여하기
userbase: KDE Eco
---
## 지속 가능한 소프트웨어 개발에 참가하기

이 의제를 계속 진행할 수 있는 의욕 있는 사람들의 도움이 필요합니다. 아래 채널을 통해서 정보를 얻고 기여할 수 있습니다.

### Community & Support

- BigBlueButton: Monthly meet-ups, 2nd Wednesdays 19:00 CEST/CET (Berlin time) (contact us for details)
- Matrix 대화방: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Energy Efficiency Mailing list: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE Eco 포럼: https://forum.kde.org/viewforum.php?f=334

### Resources

- FEEP GitLab repository: https://invent.kde.org/teams/eco/feep
- BE4FOSS GitLab repository: https://invent.kde.org/teams/eco/be4foss
- Blue Angel Applications: https://invent.kde.org/teams/eco/blue-angel-application

### 연락

이메일: `joseph [at] kde.org`
