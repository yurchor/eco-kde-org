---
layout: eco-label
title: 친환경 인증
---
2020년 독일 연방환경청(Umweltbundesamt)에서는 데스크톱 소프트웨어가 환경 친화 인증인 Blauer Engel 마크를 획득할 수 있는 조건을 발표했습니다. 인증 조건에는 에너지 효율성, 하드웨어의 기대 수명 증가, 사용자 자율성 확보 등이 있으며 ... 이 모든 것은 자유 오픈 소스 소프트웨어에 부합합니다.

The BE4FOSS project advances eco-certification for resource efficient software in the FOSS community. Obtaining the Blauer Engel label occurs in 3 steps: (1) MEASURE, (2) ANALYZE, and (3) CERTIFY.

1. MEASURE in dedicated labs, such as at KDAB Berlin
1. ANALYZE using statistical tools such as OSCAR (Open source Software Consumption Analysis in R)
1. CERTIFY by submitting the report on the fulfillment of the Blauer Engel criteria

친환경 인증의 이점은 다음과 같습니다:

- 높은 수준의 친환경적 소프트웨어 디자인 표준을 만족함을 인정
- 자유 소프트웨어를 대체재와 구별
- 사용자의 제품 선택 가능성 향상
- 소프트웨어의 환경 발자국 투명성 홍보
