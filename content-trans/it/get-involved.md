---
menu:
  main:
    weight: 4
name: KDE Eco
title: Partecipa
userbase: KDE Eco
---
## Entra a far parte del movimento del software sostenibile

Abbiamo bisogno di quante più persone motivate per portare avanti questo progetto. Ecco alcuni canali in cui è possibile ottenere maggiori informazioni e contribuire.

### Supporto e comunità

- BigBlueButton: Incontri mensili, 2° mercoledì 19:00 CEST/CET (ora di Berlino) (contattaci per i dettagli)
- Stanza Matrix: https://webchat.kde.org/#/room/#energy-efficiency:kde.org
- Lista di distribuzione di efficienza energetica: https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency
- KDE Eco Forum: https://forum.kde.org/viewforum.php?f=334

### Risorse

- Deposito GitLab FEEP: https://invent.kde.org/teams/eco/feep
- Deposito GitLab BE4FOSS: https://invent.kde.org/teams/eco/be4foss
- Applicazione Blue Angel: https://invent.kde.org/teams/eco/blue-angel-application

### Contatto

Email: `joseph [at] kde.org`
