---
SPDX-License-Identifier: CC-BY-4.0
aliases:
- ./post-1/
author: Joseph P. De Veaugh-Geiss
authors:
- SPDX-FileCopyrightText: 2022 Joseph P. De Veaugh-Geiss <joseph@kde.org>
categories:
- Software Libero
- Efficienza energetica
- Hardware Operating Life
- Blauer Engel
date: 2022-01-25
summary: L'efficienza del software è efficienza delle risorse!
title: Software efficiente in termini di risorse e Eco-certificazione Blauer Engel
url: blog/2022-01-25-resource-efficient-software-and-blauer-engel-eco-certification
---
Spesso viene trascurato il fatto che il consumo di risorse dell'hardware è determinato dal software in esecuzione su di esso.

*Quanta energia consuma l'hardware?*

Dai un'occhiata al software: la stessa macchina che svolge lo stesso compito, ma con due diverse applicazioni può avere [richieste di energia drasticamente diverse](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/2018- 12-12_texte_105-2018_ressourceneffiziente-software_0_1_.pdf).

*Quanto dura la vita operativa di un componente hardware?*

Ancora una volta, il software è fondamentale. Il sovraccarico del software che richiede hardware sempre più potente comporta la produzione e la vendita di nuovi dispositivi, mentre i dispositivi perfettamente funzionanti vengono scartati.

{{< container class="text-center" >}}

![WEEE Man al progetto Eden](/blog/images/geograph-2637892-by-James-T-M-Towill_800x600.jpg "WEEE Man al progetto Eden")

*"The seven metre tall WEEE (Waste Electrical and Electronic Equipment) Man is made of electrical and electronic equipment thrown away by the average UK citizen in their lifetime - equating to 3.3 tonnes." ([source](https://web.archive.org/web/20180901075137/https://www.geograph.org.uk/photo/2637892))*

{{< /container >}}

Quindi, quando pensiamo a cosa fare mentre la crisi climatica continua, da dove iniziare e come, noi di [KDE Eco](https://eco.kde.org/) pensiamo che il software, in particolare il software libero, sia un buon posto da cui iniziare. Software libero significa che gli utenti e le loro comunità hanno la libertà di controllare il software che usano, non il contrario. Queste libertà forniscono una scelta reale. Scegli cosa installare o disinstallare. Opzioni per modificare il software in modo che funzioni in modo più efficiente. Supporto continuo per dispositivi meno recenti, ma perfettamente funzionanti. E così via.

Ognuna di queste scelte ha un costo e/o un vantaggio. Ad esempio, più processi (non necessari) ha un'applicazione in esecuzione sullo sfondo, più risorse saranno necessarie all'hardware. Moltiplica tali costi per centinaia di milioni, o addirittura miliardi, di utenti di computer in tutto il mondo per avere rapidamente una somma.

È vero anche il contrario: si riducono le richieste del software e si riducono le richieste delle nostre risorse.

In altre parole, efficienza del software significa efficienza delle risorse!

Recentemente, l'interdipendenza tra ingegneria del software e la sostenibilità è stata ufficialmente riconosciuta dall'[Agenzia tedesca per l'ambiente](https://www.umweltbundesamt.de/en) (*Umweltbundesamt*, o UBA). Nel 2020 l'UBA ha rilasciato i criteri di aggiudicazione per ottenere la certificazione ecologica Blauer Engel per il software desktop. Le categorie per la certificazione includono l'efficienza energetica, l'estensione della vita operativa potenziale dell'hardware e l'autonomia dell'utente... tutte caratteristiche che si adattano perfettamente al software libero e open source (FOSS).

{{< container class="text-center" >}}

![Logo KDE Eco con vegetazione](/blog/images/KDE-eco-logo-name_vegetation.jpg)

{{< /container >}}

Il progetto Blauer Engel 4 FOSS ([BE4FOSS](https://invent.kde.org/teams/eco/be4foss)) di KDE e.V. cerca di raccogliere, riassumere e diffondere informazioni relative alla certificazione ecologica Blauer Engel e all'efficienza delle risorse in relazione allo sviluppo FOSS. La misurazione del consumo energetico del software libero è al centro del progetto per l'efficienza energetica del software libero e open source ([FEEP](https://invent.kde.org/teams/eco/feep)).Entrambi i progetti fanno parte dell'iniziativa pionieristica [KDE Eco](https://invent.kde.org/teams/eco)!

{{< container class="text-center" >}}

<font size="6"> **[Unisciti a noi nella creazione di software libero a efficienza energetica!](https://eco.kde.org/get-involved/)** </font>

{{< /container >}}

*Questa è una versione modificata dell'abstract [inviato](https://web.archive.org/web/20211227143613/https://pretalx.c3voc.de/rc3-2021-r3s/talk/UUJGTC/) al Remote Rhein Ruhr Stage al CCC 2021. Puoi guardare il discorso [qui](https:// streaming.media.ccc.de/rc3/relive/319).*

#### Avviso di finanziamento

Il progetto BE4FOSS è stato finanziato dall'Agenzia federale dell'ambiente e dal Ministero federale per l'ambiente, la conservazione della natura, la sicurezza nucleare e la protezione dei consumatori (BMUV<sup><a id="fnr.1" class="footref" href="#fn .1">1</a></sup>). I fondi sono messi a disposizione con delibera del Bundestag tedesco.

{{< container >}}

<img src="/blog/images/bmuv.png" alt="BMUV logo" width="340px"/>

<img src="/blog/images/uba.jpg" alt="UBA logo" width="250px"/>

{{< /container >}}

L'editore è responsabile per il contenuto di questa pubblicazione.

<sup><a id="fn.1" href="#fnr.1">1</a></sup> Official BMUV and UBA-Logos are sent only by request at: verbaendefoerderung@uba.de
